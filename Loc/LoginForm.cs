﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Loc
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();

            AccoutManager.GetInstance().SaveAccount();

            Localize();
        }

        private void Localize()
        {

        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LoginButtom_Click(object sender, EventArgs e)
        {
            string login = LoginTextBox.Text;

            if (Regex.IsMatch(login, AccoutManager.GetInstance().EmailPattern))
            {
                if (AccoutManager.GetInstance().Authorize(LoginTextBox.Text, PasswordTextBox.Text))
                {
                    new MainForm().Show();

                    Hide();
                }
                else
                {
                    MessageBox.Show(LocalizationManager.GetInstance().Localization.Data.AuthorizationFailureMessage);
                }
            }
        }

        private void LoginTextBox_TextChanged(object sender, EventArgs e)
        {
            string login = LoginTextBox.Text;

            if (login.Length != 0)
            {
                if (Regex.IsMatch(login, AccoutManager.GetInstance().EmailPattern))
                {
                    LoginLabel.ForeColor = Color.Green;
                }
                else
                {
                    LoginLabel.ForeColor = Color.Red;
                }
            }
            else
            {
                LoginLabel.ForeColor = Color.Black;
            }
        }
    }
}
