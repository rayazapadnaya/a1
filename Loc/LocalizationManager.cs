﻿using Loc.Localizations;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Loc
{
    /// <summary>
    /// Класс менеджера локализаций.
    /// Использует паттерн Singleton. 
    /// Содержит в себе текущую локализацию, список локализаций.
    /// Содержит методы загрузки локализаций, а также выбора текущей.
    /// </summary>
    public class LocalizationManager
    {
        private LocalizationManager()
        {
            // Сохраняем локализацю (полезно, если она была изменена).
            SaveLocalization(LocalizationData.RussianLocalization);
            SaveLocalization(LocalizationData.EnglishLocalization);

            // Загружаем локализации из файлов.
            LoadLocalizations();

            // Выбираем первую локализацию из списка как основную.
            SetLocalization(Localizations[0]);
        }

        private static LocalizationManager instance;

        public static LocalizationManager GetInstance()
        {
            if (instance == null)
            {
                instance = new LocalizationManager();
            }

            return instance;
        }

        // Текущая локализация.
        public Localization Localization { get; set; }

        // Список локализаций.
        public List<Localization> Localizations { get; set; }

        private readonly string rootLocalizationPath = "Localizations";

        // Метод загрузки локализаций.
        public void LoadLocalizations()
        {
            // Добавляем локализации в список.
            Localizations = new List<Localization>
            {
                LoadLocalization("Русский.xml"),
                LoadLocalization("English.xml")
            };
        }

        // Метод выбора текущей локализации.
        public void SetLocalization(Localization localization)
        {
            Localization = localization;
        }

        // Загрузка локализации.
        private Localization LoadLocalization(string path)
        {
            string tempPath = Path.Combine(rootLocalizationPath, path);

            XmlSerializer formatter = new XmlSerializer(typeof(Localization));

            using (FileStream fs = new FileStream(tempPath, FileMode.Open))
            {
                return formatter.Deserialize(fs) as Localization;
            }
        }

        // Сохранение локализации.
        public void SaveLocalization(Localization localization)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Localization));

            using (FileStream fs = new FileStream(Path.Combine(rootLocalizationPath, localization.Name + ".xml"), FileMode.Create))
            {
                formatter.Serialize(fs, localization);
            }
        }
    }
}
