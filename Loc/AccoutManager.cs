﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Loc
{
    public class AccoutManager
    {
        private AccoutManager()
        {
            SaveAccount();
            LoadAccounts();
        }

        private static AccoutManager instance;

        public static AccoutManager GetInstance()
        {
            if (instance == null)
            {
                instance = new AccoutManager();
            }

            return instance;
        }

        public string EmailPattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";

        public Account Account { get; set; }

        public List<Account> Accounts { get; set; }

        private readonly string rootAccountsPath = "Accounts/Accounts.xml";

        public bool Authorize(string login, string password)
        {
            if (Accounts.Count != 0)
            {
                Account _account = Accounts.FirstOrDefault(a => a.Email == login);

                if (_account != null)
                {
                    if (_account.Password == password)
                    {
                        Account = _account;

                        return true;
                    }
                }
            }

            return false;
        }

        public void LoadAccounts()
        {
            string tempPath = Path.Combine(rootAccountsPath);

            XmlSerializer formatter = new XmlSerializer(typeof(Account[]));

            using (FileStream fs = new FileStream(tempPath, FileMode.Open))
            {
                Accounts = (formatter.Deserialize(fs) as Account[]).ToList();
            }
        }

        public void SaveAccount()
        {
            List<Account> _accounts = new List<Account>
            {
                new Account
                {
                    Email = "rayazapadnaya@gmaol.com",
                    Password = "12345"
                },
                new Account
                {
                    Email = "dmitrygrach@gmaol.com",
                    Password = "54321"
                },
                new Account
                {
                    Email = "1@gmaol.com",
                    Password = "1"
                }
            };

            XmlSerializer formatter = new XmlSerializer(typeof(List<Account>));

            using (FileStream fs = new FileStream(Path.Combine(rootAccountsPath), FileMode.Create))
            {
                formatter.Serialize(fs, _accounts);
            }
        }
    }
}
