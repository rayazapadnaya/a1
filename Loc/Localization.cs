﻿namespace Loc
{
    /// <summary>
    /// Класс модели локализации.
    /// Содержет название локализации и его данные.
    /// </summary>
    public class Localization
    {
        public string Name { get; set; }
        public Data Data { get; set; }
    }

    /// <summary>
    /// Класс данных подлежащих локализации.
    /// </summary>
    public class Data
    {
        public string LocalizationLabel { get; set; }
        public string ApplyLocaliazationButton { get; set; }
        public string CloseButton { get; set; }
        public string WindowButton { get; set; }
        public string HiLabel { get; set; }
        public string AuthorizationFailureMessage { get; set; }
    }
}
