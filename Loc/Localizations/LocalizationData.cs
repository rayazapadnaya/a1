﻿namespace Loc.Localizations
{
    /// <summary>
    /// Класс, который содержит доступную локализацию.
    /// </summary>
    internal class LocalizationData
    {
        public static readonly Localization RussianLocalization = new Localization
        {
            Name = "Русский",
            Data = new Data
            {
                ApplyLocaliazationButton = "Применить",
                CloseButton = "Закрыть",
                LocalizationLabel = "Выберите локализацию",
                WindowButton = "Открыть окно",
                HiLabel = "Привет",
                AuthorizationFailureMessage = "Ошибка авторизации"
            }
        };

        public static readonly Localization EnglishLocalization = new Localization
        {
            Name = "English",
            Data = new Data
            {
                ApplyLocaliazationButton = "Apply",
                CloseButton = "Close",
                LocalizationLabel = "Choose localization",
                WindowButton = "Open the window",
                HiLabel = "Henlo",
                AuthorizationFailureMessage = "Autorization error"
            }
        };
    }
}
