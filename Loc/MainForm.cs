﻿using System;
using System.Windows.Forms;

namespace Loc
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            // Ставим источник элементов для выпадающего списка.
            LocalizationsComboBox.DataSource = LocalizationManager.GetInstance().Localizations;
            // Ставим свойство для отображения в выпадающем списке.
            LocalizationsComboBox.DisplayMember = "Name";

            // Выполняем первоначальную привязку локализации.
            Localize();
        }

        // Метод привязки локализации к компонентам формы.
        public void Localize()
        {
            CloseButton.Text = LocalizationManager.GetInstance().Localization.Data.CloseButton;
            ApplyLocaliazationButton.Text = LocalizationManager.GetInstance().Localization.Data.ApplyLocaliazationButton;
            LocalizationLabel.Text = LocalizationManager.GetInstance().Localization.Data.LocalizationLabel;
            WindowButton.Text = LocalizationManager.GetInstance().Localization.Data.WindowButton;
        }

        // Реализация события нажатия на кнопку "Применить".
        private void ApplyLocaliazationButton_Click(object sender, EventArgs e)
        {
            // Если текущий выбранный элемент существует, передаем его в метод выбора текущей локализации.
            if (LocalizationsComboBox.SelectedItem != null)
            {
                LocalizationManager.GetInstance().SetLocalization(LocalizationsComboBox.SelectedItem as Localization);

                // Выполняем привязку локализации.
                Localize();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(LocalizationManager.GetInstance().Localization.Data.HiLabel);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
