﻿namespace Loc
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LocalizationsComboBox = new System.Windows.Forms.ComboBox();
            this.ApplyLocaliazationButton = new System.Windows.Forms.Button();
            this.LocalizationLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.WindowButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LocalizationsComboBox
            // 
            this.LocalizationsComboBox.FormattingEnabled = true;
            this.LocalizationsComboBox.Location = new System.Drawing.Point(12, 34);
            this.LocalizationsComboBox.Name = "LocalizationsComboBox";
            this.LocalizationsComboBox.Size = new System.Drawing.Size(356, 24);
            this.LocalizationsComboBox.TabIndex = 0;
            // 
            // ApplyLocaliazationButton
            // 
            this.ApplyLocaliazationButton.Location = new System.Drawing.Point(374, 33);
            this.ApplyLocaliazationButton.Name = "ApplyLocaliazationButton";
            this.ApplyLocaliazationButton.Size = new System.Drawing.Size(107, 25);
            this.ApplyLocaliazationButton.TabIndex = 1;
            this.ApplyLocaliazationButton.Text = "Применить";
            this.ApplyLocaliazationButton.UseVisualStyleBackColor = true;
            this.ApplyLocaliazationButton.Click += new System.EventHandler(this.ApplyLocaliazationButton_Click);
            // 
            // LocalizationLabel
            // 
            this.LocalizationLabel.AutoSize = true;
            this.LocalizationLabel.Location = new System.Drawing.Point(12, 9);
            this.LocalizationLabel.Name = "LocalizationLabel";
            this.LocalizationLabel.Size = new System.Drawing.Size(166, 17);
            this.LocalizationLabel.TabIndex = 2;
            this.LocalizationLabel.Text = "Выберите локализацию";
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(13, 430);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(92, 25);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // WindowButton
            // 
            this.WindowButton.Location = new System.Drawing.Point(355, 428);
            this.WindowButton.Name = "WindowButton";
            this.WindowButton.Size = new System.Drawing.Size(126, 25);
            this.WindowButton.TabIndex = 4;
            this.WindowButton.Text = "Открыть окно";
            this.WindowButton.UseVisualStyleBackColor = true;
            this.WindowButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 465);
            this.Controls.Add(this.WindowButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.LocalizationLabel);
            this.Controls.Add(this.ApplyLocaliazationButton);
            this.Controls.Add(this.LocalizationsComboBox);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox LocalizationsComboBox;
        private System.Windows.Forms.Button ApplyLocaliazationButton;
        private System.Windows.Forms.Label LocalizationLabel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button WindowButton;
    }
}

